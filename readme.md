Para usar, configure seu arquivo .env localmente, gere uma chave com o artisan, instale os modulos do composer e do node necessários, execute as migrações e suba o server.

Ao acesar, você irá se deparar com um painel CRUD do desafio 1.
No menu, existe uma opção chamada "Exportar tudo". É a solução do desafio 2.
O Desafio 3, infelizmente, não tive tempo hábil para desenvolver, mas, através da interface desenvolvida para o painel em questão, é possível avaliar algumas das minhas habilidades com front end e criação.

Explicando a solução do desafio 2:
Fiz no server side uma página que processa as informações e gera o xml sem tempo limite de execução (O ideal, seria conhecer o tempo máximo que o sistema pode levar e setar com uma margem de segurança). Essa página é chamada dentro da aplicação via ajax de forma assíncrona para não atrapalhar outros usos no sistema, e após pronto o arquivo, eu faço uma chamada requisitando o download do arquivo. Muitos outros tratamentos poderiam ser feitos, como por exemplo aguardar corretamente um callback da função que gera o arquivo, ou um melhor tratamento de erros dessa parte, mas, acredito que dessa forma, é suficiente para cumprir o desafio dentro do que foi pedido dentro do prazo estipulado.
