<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;

class Team extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teams = \App\Team::all();
        return view('teams', compact('teams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('team-register');
    }

    /**
     * Export teams data to a csv file.
     *
     * @return \Illuminate\Http\Response
     */
    public function export()
    {
        return Excel::download(new \App\Exports\TeamsExport, 'times.csv');
    }

    /**
     * Export teams data to a xml file.
     *
     * @return \Illuminate\Http\Response
     */
    public function exportxml()
    {
        set_time_limit(0);
        $teams = new \App\Team();
        $teams = $teams->with('players')->get();
        $x=new \XMLWriter();
        $x->openMemory();
        $x->startDocument('1.0','UTF-8');
        $x->startElement('teams');
        foreach ($teams as $team) {

            $x->startElement('team');
            $x->writeAttribute('name',$team['name']);

            foreach ($team['players'] as $player) {

                $x->startElement('player');
                $x->writeAttribute('name',$player['name']);
                $x->writeAttribute('shirt_number',$player['shirt_number']);
                $x->endElement(); // player
            } // foreach $players

            $x->endElement(); // team
        } // foreach $teams

        $x->endElement(); // teams
        $x->endDocument();
        $xml = $x->outputMemory();

        \Storage::disk('local')->put('teams.xml', $xml);
        return response()->json(['success' => 'success', 'filename'=>'teams.xml'], 200);
    }

    public function downloadxml($filename)
    {
        return \Storage::download($filename);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = $request->validate([
            'name' => 'required',
            'city' => 'required'
        ]);
        $team = \App\Team::create($validation);
        return redirect('/teams')->with('success', 'Time Adicionado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $team = \App\Team::findOrFail($id);
        return view('team-register', compact('team'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation = $request->validate([
            'name' => 'required',
            'city' => 'required'
        ]);

        \App\Team::whereId($id)->update($validation);

        return redirect('/teams')->with('success', 'Time Salvo');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $team = \App\Team::findOrFail($id);
        $team->delete();

        return redirect('/teams')->with('success', 'Time deletado da sua base');
    }
}
