<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Player extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('team')){
            $filter = array(
                'team_id' => $request->input('team')
            );
            $players = new \App\Player();
            $players = $players->search($filter);
        }else{
            $players = \App\Player::all();
        }

        return view('players', compact('players'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $teams = \App\Team::all();
        return view('player-register', compact('player', 'teams'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = $request->validate([
            'name' => 'required',
            'shirt_number' => 'required',
            'team_id' => 'required'
        ]);
        $player = \App\Player::create($validation);
        return redirect('/players')->with('success', 'Jogador Adicionado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $player = \App\Player::findOrFail($id);
        $teams = \App\Team::all();

        return view('player-register', compact('player', 'teams'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation = $request->validate([
            'name' => 'required',
            'shirt_number' => 'required',
            'team_id' => 'required'
        ]);

        \App\Player::whereId($id)->update($validation);

        return redirect('/players')->with('success', 'Jogador Salvo');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $player = \App\Player::findOrFail($id);
        $player->delete();

        return redirect('/players')->with('success', 'Jogador deletado da sua base');
    }
}
