<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    protected $fillable = ['name', 'shirt_number', 'team_id'];

    public function Team(){
        return $this->belongsTo(Team::class);
    }

    public function search($filter = NULL){
        return $this
        ->when($filter['team_id'], function($query, $termo){
            $query->where('team_id', $termo);
        })
        ->get();
    }
}
