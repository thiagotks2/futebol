<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('teams/export', 'Team@export')->name('teams.export');
Route::get('teams/exportxml', 'Team@exportxml')->name('teams.exportxml');
Route::get('teams/downloadxml/{filename}', 'Team@downloadxml')->name('teams.downloadxml');

Route::get('/', 'Team@index');
Route::resource('teams', 'Team');
Route::resource('players', 'Player');

