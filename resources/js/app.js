WebFontConfig = {
    google: {
        families: ['Quicksand:400,700']
    }
};

(function(d) {
    var wf = d.createElement('script'), s = d.scripts[0];
    wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js';
    wf.async = true;
    s.parentNode.insertBefore(wf, s);
})(document);

window.addEventListener('load', function () {
    var alerts = document.querySelectorAll(".message");
    for (var i = 0; i < alerts.length; i++) {
        alerts[i].addEventListener('click', function(e) {
            e.target.style.opacity = 0;
            e.target.style.right = "-400px";
        });
    }
    document.getElementById('download-all').addEventListener('click', function(){
        $.ajax({
            url: 'teams/exportxml/',
            dataType: 'html',
            type: 'GET',
            async: true,
            success: function(data){
                data = JSON.parse(data);
                if(data.success == 'success'){
                    window.open('teams/downloadxml/'+data.filename);
                }
            }
        });
    });
}, false);
