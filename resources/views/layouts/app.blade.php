<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    @yield('css')
    <title>@yield('title') - Futebol</title>
</head>
<body>
        <div class="navigation">
            <nav>
                <a href="/" class="logo">Início</a>
                <a href="{{ route('teams.index') }}" class="btn btn-outline-light">Times</a>
                <a href="{{ route('players.index') }}" class="btn btn-outline-light">Jogadores</a>
                <div id="download-all" class="btn btn-outline-light">Exportar Tudo</div>
            </nav>
        </div>

        <div class="content">
            @yield('conteudo')
        </div>




    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>

</body>
</html>
