@section('title', 'Times')

@extends('layouts.app')

@section('conteudo')

    @if(session()->get('success'))
        <div class="message success">
        {{ session()->get('success') }}
        </div>
    @endif

    @if ($errors->any())
        <div class="message error">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="topo">
        <h1>Times</h1>
        <div class="opcoes-top">
            <a href="{{ route('teams.export') }}" class="btn"><span>></span> Exportar CSV</a>
            <a href="{{ route('teams.create') }}" class="btn color"><span>+</span> Novo Time</a>
        </div>
    </div>


    <div class="teams card-group">

        @if(isset($teams) && count($teams) > 0)

            @foreach($teams as $team)

                <div class="card shadow">
                    <div class="card-image">
                        <h2 class="card-title">{{ $team->name }}</h2>
                        <p class="card-description">{{ $team->city }}</p>
                    </div>
                    <div class="opcoes">
                            <form action="{{ route('teams.destroy' , $team->id)}}" method="POST">
                                <input name="_method" type="hidden" value="DELETE">
                                {{ csrf_field() }}

                                <button type="submit" class="btn">Excluir</button>
                            </form>
                        <a href="{{ route('teams.edit', $team->id) }}" class="btn">Editar</a>
                        <a href="{{ route('players.index', ['team' => $team->id]) }}" class="btn">Ver Jogadores</a>
                    </div>
                </div>

            @endforeach

        @else

            <h2>Nada por aqui</h2>

        @endif

    </div>

@endsection

