@section('title', 'Jogadores')

@section('css')
    <link rel="stylesheet" href="{{ asset('css/form.css') }}">
@stop

@extends('layouts.app')

@section('conteudo')

    @if(session()->get('success'))
        <div class="message success">
        {{ session()->get('success') }}
        </div>
    @endif

    @if ($errors->any())
        <div class="message error">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="topo">
        <h1>@if(isset($player)) Editar Jogador: {{ $player->name }}  @else Adicionar Jogador @endif</h1>
    </div>


    <form method="post" action="{{ (isset($player)) ? route('players.update', $player->id) : route('players.store') }}" class="card form shadow">
        @csrf
        @if(isset($player))
            @method('PATCH')
        @endif
        <input name="id" type="hidden" value="{{ (isset($player->id)) ? $player->id : '' }}">
        <div class="input-container">
          <input placeholder=" " id="iName" type="text" name="name" required value="{{ (isset($player->name)) ? $player->name : '' }}">
          <label for="iName">Nome do Jogador *</label>
        </div>
        <div class="input-container">
            <input placeholder=" " id="iShirtNumber" type="text" name="shirt_number" required value="{{ (isset($player->shirt_number)) ? $player->shirt_number : '' }}">
            <label for="iShirtNumber">Número da Camisa *</label>
        </div>
        <div class="input-container">
            <select class="form-control" id="team_id" name="team_id" required>
                @foreach($teams as $team)
                    <option {{ (isset($player) && $player->team_id == $team->id) ? 'selected' : '' }} value="{{ $team->id }}">{{ $team->name }}</option>
                @endforeach
            </select>
            <label for="team_id">Pertence ao Time *</label>
        </div>
        <div class="opcoes">
            <a href="javascript:history.back()" class="btn">Cancelar</a>
            <button type="submit" class="btn color">Salvar</button>
        </div>
      </form>

@endsection

