@section('title', 'Times')

@section('css')
    <link rel="stylesheet" href="{{ asset('css/form.css') }}">
@stop

@extends('layouts.app')

@section('conteudo')

    @if(session()->get('success'))
        <div class="message success">
        {{ session()->get('success') }}
        </div>
    @endif

    @if ($errors->any())
        <div class="message error">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="topo">
        <h1>@if(isset($team)) Editar time: {{ $team->name }}  @else Adicionar Time @endif</h1>
    </div>


    <form method="post" action="{{ (isset($team)) ? route('teams.update', $team->id) : route('teams.store') }}" class="card form shadow">
        @csrf
        @if(isset($team))
            @method('PATCH')
        @endif
        <input name="id" type="hidden" value="{{ (isset($team->id)) ? $team->id : '' }}">
        <div class="input-container">
          <input placeholder=" " id="iNome" type="text" name="name" required value="{{ (isset($team->name)) ? $team->name : '' }}">
          <label for="iNome">Nome do Time *</label>
        </div>
        <div class="input-container">
            <input placeholder=" " id="iCity" type="text" name="city" required value="{{ (isset($team->city)) ? $team->city : '' }}">
            <label for="iCity">Cidade *</label>
        </div>
        <div class="opcoes">
            <a href="javascript:history.back()" class="btn">Cancelar</a>
            <button type="submit" class="btn color">Salvar</button>
        </div>
      </form>

@endsection

