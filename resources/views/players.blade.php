@section('title', 'Jogadores')

@extends('layouts.app')

@section('conteudo')

    @if(session()->get('success'))
        <div class="message success">
        {{ session()->get('success') }}
        </div>
    @endif

    @if ($errors->any())
        <div class="message error">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="topo">
        <h1>Jogadores</h1>
        <div class="opcoes-top">
            <a href="{{ route('players.create') }}" class="btn color"><span>+</span> Novo Jogador</a>
        </div>

    </div>


    <div class="players card-group">

        @if(isset($players) && count($players) > 0)

            @foreach($players as $player)

                <div class="card shadow">
                    <div class="card-image">
                        <h2 class="card-title">{{ $player->name }}</h2>
                        <p class="card-description">Camisa nº {{ $player->shirt_number }}</p>
                    </div>
                    <div class="opcoes">
                        <form action="{{ route('players.destroy' , $player->id)}}" method="POST">
                            <input name="_method" type="hidden" value="DELETE">
                            {{ csrf_field() }}

                            <button type="submit" class="btn">Excluir</button>
                        </form>
                        <a href="{{ route('players.edit', $player->id) }}" class="btn">Editar</a>
                    </div>
                </div>

            @endforeach

        @else

            <h2>Nada por aqui</h2>

        @endif

    </div>

@endsection

